
// keep track of HTML tags
var tagCount = {}

// this function is called after html page
// is loaded
function executeFetchTags(){
  countTags(fetchTags());
  appendToBody();
};

//iterate over tags and build tagCount object
function countTags(tags){
  $.each(tags, function( index, value ) {
    var name = value.localName;
    if (tagCount.hasOwnProperty(name)){
      tagCount[name] = tagCount[name] + 1
    }
    else{
      tagCount[name] = 1
    }
  });
};

// fetch all required tags on page using Jquery
function fetchTags(){
  return $("h1, h2, h3, p, a");
};

// generate dynamic html table and append to body
function appendToBody(){
  var content = "<div id=customTable>";
  content    += "<table class=myTable>";
  content    += '<tr class=row><td class=data> Tag </td>';
  content    += '<td class=data> Count </td></tr>';

  $.each( tagCount, function( key, value ) {
    content += '<tr class=row>';
    content += '<td class=data>' + key + '</td>';
    content += '<td class=data>' + value + '</td>';
    content += '</tr>';
  });

  content += "</table></div>";
  $("body").append(content);
};

// fetch and count tags after page is ready
$(document).on('ready', function(){
  executeFetchTags();
});
