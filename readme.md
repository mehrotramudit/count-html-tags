## Chrome extension to count HTML tags

This extension will count following HTML tags `<h1>, <h2>, <h3>, <p> <a>` and display a table in bottom of webpage you visit.

## How to use extension


1. clone this repository

    ```
    git clone git@bitbucket.org:mehrotramudit/count-html-tags.git
    ```
   
   
2. in chrome browser goto [chrome://extensions/](chrome://extensions/) and checkmark `Developer mode` on top right
   

3. click `Load unpacked extension...` and point it to repository you just cloned

    ![Image: where to upload extension ](http://i.imgur.com/ZJDzyGp.png)
   
   
4. now visit any web page and you will see a table in bottom with HTML tag count
   
   
    * <https://www.atlassian.com/software/jira/whats-new>
          
        ![Image: Tag count table rendered in bottom of Altassian page](http://i.imgur.com/4eLpV04.png)
           
           
    * <http://www.reddit.com/>
        
        ![Image: Tag count table rendered in bottom of Reddit homepage](http://i.imgur.com/nVqPKeC.png)
    